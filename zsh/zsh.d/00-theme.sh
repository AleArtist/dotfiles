#!/bin/bash

# if no theme engine is specified, default to p9k
if [ -z ${ZSH_THEME_ENGINE} ]; then
  export ZSH_THEME_ENGINE="p9k"
fi
# load default p9k theme if none is specified
if [ "${ZSH_THEME_ENGINE}" = "p9k" ]; then
if [ -z "$ZSH_P9K_THEME" ]; then
  ZSH_P9K_THEME="default"
fi

. ~/zsh.d/themes/$ZSH_P9K_THEME.sh
fi
