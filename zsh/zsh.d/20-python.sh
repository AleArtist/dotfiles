#!/bin/bash
alias pyt='python -m pytest tests'
alias pytc='python -m pytest tests --cov=.'
alias pytcr='python -m pytest tests --cov=. --cov-report=html'
