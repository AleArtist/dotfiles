#!/bin/bash
function confedit(){
  ranger ~/.config/"${1}"
}
function p9k_theme(){
  theme="${1}"
  if [ -z "${theme}" ]; then
    theme="default"
  fi
  if [ ! -f ~/zsh.d/.local/.localrc ]; then
    touch ~/zsh.d/.local/.localrc
    echo ZSH_P9K_THEME=\"${theme}\" >> ~/zsh.d/.local/.localrc
    echo 'source ~/zsh.d/themes/$ZSH_P9K_THEME.sh' >> ~/zsh.d/.local/.localrc
    echo "Created local configuration file at ~/zsh.d/.local/.localrc"
  else
    if [ "$(uname)" != "Darwin" ]; then
      sed -i "s/ZSH_P9K_THEME=\"[a-zA-Z0-9]*\"/ZSH_P9K_THEME=\"${theme}\"/" ~/zsh.d/.local/.localrc
    else
      sed -i '' "s/ZSH_P9K_THEME=\"[a-zA-Z0-9]*\"/ZSH_P9K_THEME=\"${theme}\"/" ~/zsh.d/.local/.localrc
    fi
  fi
  . ~/.zshrc
}
function uecho(){
  local unicode="\u${1}"
  echo "${unicode}"
}
function venv(){
  . ~/.venv/"${1}"/bin/activate
}
function mkcd(){
  folder=${1}
  mkdir -pv "${folder}"; cd "${folder}" || return
}
function mkt(){
  tempdir="$(mktemp -d)"
  cd "${tempdir}" || return
  unset tempdir
}
