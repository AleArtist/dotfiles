function ec2ip(){
aws ec2 describe-instances --filters "Name=tag:Name,Values=${1}" | jq '.Reservations[0].Instances[0].NetworkInterfaces[0].Association.PublicIp'
}

