#!/bin/sh
# Call with configure-git.sh "Username" "Emailaddress"
git config --global user.name "$1"
git config --global user.email "$2"
git config --global core.autocrlf input
git config --global core.editor nvim
git config --global alias.br branch
git config --global alias.st status
git config --global alias.sac "!git stash apply; git stash clear"
git config --global alias.amd "cz --amend"
git config --global alias.a "add ."

gpg_id=$(gpg --list-secret-keys --keyid-format LONG | grep sec | grep -Eo '(/[A-Z0-9]*)\s' | cut -d "/" -f 2)
if [ "$gpg_id" ]; then
	git config --global commit.gpgsign true
	git config --global user.signingkey $gpg_id
fi
echo '{"path": "cz-conventional-changelog"}' > ~/.czrc

