#!/bin/bash
# load all plugins
. <(antibody init)
antibody bundle < ~/zsh.d/.antibody_plugins
if [ "${ZSH_THEME_ENGINE}" = "p9k" ]; then
  antibody bundle bhilburn/powerlevel9k branch:next
elif [ "${ZSH_THEME_ENGINE}" = "pure" ]; then
  antibody bundle sindresorhus/pure
fi
