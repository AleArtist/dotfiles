#!/bin/bash
dkb () {
	registry="dockerwebplus.azurecr.io" 
	imagename="${1}" 
	tag="${2}" 
	docker build -t "${registry}"/"${imagename}":"${tag}" "${3}"
}
dkp () {
	registry="dockerwebplus.azurecr.io" 
	imagename="${1}" 
	tag="${2}" 
	docker push "${registry}"/"${imagename}":"${tag}"
}
dkbp () {
	imagename="${1}" 
	tag="${2}" 
	dkb "${imagename}" "${tag}" "${3}"
	dkp "${imagename}" "${tag}"
}
dkbpt () {
	imagename="${1}" 
	tag="${$(git describe)//v}"
	dkb "${imagename}" "${tag}" "${2}"
	dkp "${imagename}" "${tag}"
}
