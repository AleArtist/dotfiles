#!/bin/bash
kubedash(){
  az aks browse --name "${K8S_AZ_RESOURCE_NAME}" --resource-group "${K8S_AZ_RESOURCE_GROUP}"
}
kubeswitch(){
  kubefile=~/.kubeswitch/${1}.kube
  if [ -e ${kubefile} ]; then

  subscription="$(grep 'subscription' ${kubefile} | awk '{ print $2; }')"
  group="$(grep 'group' ${kubefile} | awk '{ print $2; }')"
  resource="$(grep 'resource' ${kubefile} | awk '{ print $2; }')"
    az account set -s "${subscription}"
    K8S_AZ_RESOURCE_GROUP="${group}"
    K8S_AZ_RESOURCE_NAME="${resource}"
  echo "K8S_AZ_RESOURCE_GROUP='${K8S_AZ_RESOURCE_GROUP}'" > ~/.az-context
  echo "K8S_AZ_RESOURCE_NAME='${K8S_AZ_RESOURCE_NAME}'" >> ~/.az-context
  kubectx "${1}"
else
  echo "Error: ${kubefile} not found!"
fi
}
