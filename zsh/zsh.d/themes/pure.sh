#!/bin/bash
# General config
P9K_MODE='nerdfont-complete'

local li0_background="1"
local li0_foreground="black"
local li1_background="black"
local li1_foreground="11"

P9K_LEFT_PROMPT_ELEMENTS=( time user_joined host_joined dir_joined vcs)
P9K_RIGHT_PROMPT_ELEMENTS=(status)

P9K_DIR_SHORTEN_LENGTH=2
P9K_DIR_SHORTEN_DELIMITER=..
P9K_PROMPT_ADD_NEWLINE=true
P9K_MULTILINE_FIRST_PROMPT_PREFIX_ICON=""
P9K_MULTILINE_NEWLINE_PROMPT_PREFIX_ICON=""
P9K_MULTILINE_LAST_PROMPT_PREFIX_ICON=$'\uF101 '
P9K_STATUS_OK=false
P9K_TIME_FORMAT='%D{%H:%M}'

# Colors
P9K_TIME_BACKGROUND="$li1_background"
P9K_TIME_FOREGROUND="$li1_foreground"
P9K_HOST_DEFAULT_BACKGROUND="$li1_background"
P9K_HOST_DEFAULT_FOREGROUND="$li1_foreground"
P9K_HOST_REMOTE_BACKGROUND="$li1_background"
P9K_HOST_REMOTE_FOREGROUND="$li1_foreground"
P9K_USER_DEFAULT_BACKGROUND="$li1_background"
P9K_USER_DEFAULT_FOREGROUND="$li1_foreground"
P9K_USER_ROOT_BACKGROUND="$li1_background"
P9K_USER_ROOT_FOREGROUND="$li1_foreground"
P9K_USER_REMOTE_BACKGROUND="$li1_background"
P9K_USER_REMOTE_FOREGROUND="$li1_foreground"
P9K_STATUS_ERROR_CR_BACKGROUND="$li1_background"
P9K_STATUS_ERROR_CR_FOREGROUND="$li1_foreground"

# Dir Colors
P9K_DIR_DEFAULT_BACKGROUND="$li1_background"
P9K_DIR_HOME_BACKGROUND="$li1_background"
P9K_DIR_HOME_SUBFOLDER_BACKGROUND="$li1_background"
P9K_DIR_ETC_BACKGROUND="$li1_background"

P9K_DIR_DEFAULT_FOREGROUND="$li1_foreground"
P9K_DIR_HOME_FOREGROUND="$li1_foreground"
P9K_DIR_HOME_SUBFOLDER_FOREGROUND="$li1_foreground"
P9K_DIR_ETC_FOREGROUND="$li1_foreground"

# Icons
P9K_DIR_ETC_ICON=$'\uf0ad'


# VCS colors
P9K_VCS_CLEAN_FOREGROUND="$li0_background"
P9K_VCS_CLEAN_BACKGROUND="$li0_foreground"
P9K_VCS_UNTRACKED_FOREGROUND="$li1_background"
P9K_VCS_UNTRACKED_BACKGROUND="$li1_foreground"
P9K_VCS_MODIFIED_FOREGROUND='white'
P9K_VCS_MODIFIED_BACKGROUND='160'
P9K_VCS_SHOW_CHANGESET=true
