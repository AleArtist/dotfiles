#!/bin/bash
if [[ $commands[kubectl] ]]; then
  . <(kubectl completion zsh)
fi

eval $(thefuck --alias)

autoload -z edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line

[ -f ~/.fzf.zsh ] && . ~/.fzf.zsh
