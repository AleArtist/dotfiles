#!/bin/sh
fpath=(~/.comp ${fpath})

# Lines configured by zsh-newuser-install
bindkey -e

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit 
compinit -C
# if [[ -n ${ZDOTDIR}/.zcompdump(#qN.mh+24) ]]; then
#     compinit;
# else
#     compinit -C;
# fi;
# End of lines added by compinstall

[ -f ~/zsh.d/.local/.localrc ] && . ~/zsh.d/.local/.localrc

# load additional scripts, functions and aliases
for file in ~/zsh.d/*.sh; do
  . "${file}"
done
# load local non-vcs-tracked scripts, functions and aliases
for file in ~/zsh.d/.local/*.sh; do
  . "${file}"
done

. ~/.zsh-autostart_tmux
