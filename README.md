# dotfiles

## Requirements
- `stow` if you don't want to symlink every file yourself
- [antibody for zsh](https://getantibody.github.io/install)

## Configuration Packages

### Terminal-based

- `nvim` / `vim`
- `tmux`
- `zsh`
- `newsboat`
- `ranger`


### Graphical

- dunst
- i3
- polybar

Additionally there are a few systemd services and targets in the `systemd` folder

## Setup
1. Clone the repository to ~/.dotfiles -> `git clone gitlab.com/sambadevi/dotfiles ~/.dotfiles`
2. Every folder is named after the program or module it corresponds with. If you only want the nvim settings (for example) use `stow nvim` to symlink `~/.dotfiles/nvim` to `~/.config/nvim/`
3. Repeat for every module you would like to use

## Git Setup
You can call `configure-git.sh` with your username and email to set the global git configuration:
```
./configure-git.sh "myUsername" "myEmail"
```
If you have a gpg key (or multiple) set, the script will automatically assign the first key to sign your commits

Afterwards check your git config with `git config -l`
