#!/bin/bash
# if exa is installed, use exa instead of ls
if [ -x "$(command -v exa)" ]; then
    alias ll='exa -laahF --git --icons'
    alias l='exa -lh --git --icons'
    alias la='exa -laah --git --icons'
    alias ls='exa --icons'
    alias lsa='exa -lah --git --icons'
else
    alias l='ls -lah'   
    alias la='ls -lAh'  
    alias ls='ls -G'    
    alias lsa='ls -lah' 
    alias ll='ls -lispa'
fi

alias fzf="fzf --preview 'head -100 {}'"
alias tmuxn='tmux new -s ${$(basename $(pwd))//./-}'

alias dotedit='ranger ~/.dotfiles'

alias vim="nvim"
alias v="nvim"

alias du="du -ach | sort -h"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
alias mkdir="mkdir -pv"
alias dots="cd ~/.dotfiles"
alias nb="newsboat"
