#!/bin/bash
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS

typeset -gA ZSH_HIGHLIGHT_STYLES

ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=white'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=122'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=112'

export PAGER=less
export EDITOR=nvim
export FZF_CTRL_T_OPTS='--height 100% --preview "coderay {}"'
export GPG_TTY=$(tty)
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH=$HOME/.dotfiles/bin:$PATH
export PATH=$HOME/local/bin:$PATH
export PATH=$HOME/.local/bin:$PATH
export PATH=$HOME/.gem/ruby/2.5.0/bin:$PATH
export PATH=${GOPATH//://bin:}/bin:$PATH
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
export PATH=$PATH:$HOME/bin
export PASSWORD_STORE_X_SELECTION=primary

# load external env files from HOME
test -f ~/.env && source ~/.env
test -f ~/.zsh_env && source ~/.zsh_env
test -f ~/.zshenv && source ~/.zshenv
